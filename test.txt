Proxy - Padrão de Projeto

O padrão Proxy tem como objetivo proporcionar um espaço
reservado para outro objeto controlar o acesso a ele.
A classe proxy teoricamente pode se conectar a qualquer
objeto, ou seja, normalmente quando existe uma instância
grande/complexa pode-se criar vários proxies, todos contendo
uma única referencia.